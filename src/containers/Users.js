import React, {Component} from 'react';
import { Router, Route, Link } from 'react-router-dom';
import {connect} from 'react-redux';

import {userActions} from '../actions/userActions';

class Users extends Component {

  componentDidMount() {
    this.props.dispatch(userActions.getAll());
  }

  handleDeleteUser(id) {
    return (e) => this.props.dispatch(userActions.delete(id));
  }

  render() {
    const {users, user} = this.props;
    console.log(user);

    return (
      <div>
        <h3>All registered users:</h3>
        {users.loading && <em>Loading users...</em>}
        {/*{users.items &&*/}
        {/*<ul>*/}
          {/*{users.items.map((user, index) =>*/}
            {/*<li key={user.id}>*/}
              {/*{user.firstName + ' ' + user.lastName}*/}
              {/*{*/}
                {/*user.deleting ? <em> - Deleting...</em>*/}
                  {/*: user.deleteError ? <span className="error"> - ERROR: {user.deleteError}</span>*/}
                  {/*: <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>*/}
              {/*}*/}
            {/*</li>*/}
          {/*)}*/}
        {/*</ul>*/}
        {/*}*/}
        {user.isAdmin ?
        <table class="table">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col"></th>
          </tr>
          </thead>
          {users.items &&
          <tbody>
          {users.items.map((user, index) =>
              <tr key={user.id}>
                <th scope="row">{index + 1}</th>
                <td>{user.firstName}</td>
                <td>{user.lastName}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                <td>{
                  user.deleting ? <em>  Deleting...</em>
                    : user.deleteError ? <span className="error">  ERROR: {user.deleteError}</span>
                    : <span>  <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                }</td>
              </tr>
          )}
          </tbody>
          }
        </table> : 'You do not have authority to see this page'}
      </div>

    )
  }
}

function mapStateToProps(state) {
  const {users, authentication} = state;
  const {user} = authentication;
  return {
    user,
    users
  };
}

const connectedUsers = connect(mapStateToProps)(Users);
export {connectedUsers as Users};