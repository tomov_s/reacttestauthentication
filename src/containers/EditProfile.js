import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {userActions} from '../actions/userActions';

class EditProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        firstName: '',
        lastName: '',
        username: '',
        email: '',
        phone: ''
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(userActions.getAll());
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps);
    this.setState({
      user: nextProps.user
    })
  }

  handleDeleteUser(id) {
    return (e) => this.props.dispatch(userActions.delete(id));
  }

  // handleChange(event) {
  //   const {value} = event.target;
  //   const {name} = event.target.name;
  //   const {user} = this.props;
  //   console.log('user', user.firstName);
  //   console.log('event', event.target.value);
  //   this.setState({
  //     user: {
  //       [name]: value
  //     }
  //   });
  // }

  handleChange(event) {
    console.log(event.target.value)
    const name = event.target.name;
    const value = event.target.value;
    let user = this.state.user;
    user[name] = value;

    this.setState({
      user: user
    })
  }

  saveChanges(e) {
    console.log(3, this.state.user);
    this.props.dispatch(userActions.update(this.state.user));
  }

  render() {
    const {user, users} = this.props;
    console.log('user', user);
    return (
      <div>
        <div className="col-md-6 col-md-offset-3">
          <h4>Your profile</h4>
          <h1>Hi {user.firstName}!</h1>
          <h4>First name: <input onChange={this.handleChange.bind(this)} value={this.state.user.firstName} name="firstName"/></h4>
          <h4>Last name: <input onChange={this.handleChange.bind(this)} value={this.state.user.lastName} name="lastName"/></h4>
          <h4>Username: <input onChange={this.handleChange.bind(this)} value={this.state.user.username} name="username"/></h4>
          <h4>Email: <input onChange={this.handleChange.bind(this)} value={this.state.user.email} name="email"/></h4>
          <h4>Phone: <input onChange={this.handleChange.bind(this)} value={this.state.user.phone} name="phone"/></h4>
          <p>You're logged in!</p>
          <p>
            <button onClick={this.saveChanges.bind(this)} className="btn btn-primary">Save changes</button>
          </p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log('state', state);
  const {users, authentication} = state;
  const {user} = authentication;
  return {
    user,
    users
  };
}

const connectedEditProfile = connect(mapStateToProps)(EditProfile);
export {connectedEditProfile as EditProfile};