import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Phone, {
  formatPhoneNumber,
  parsePhoneNumber,
  isValidPhoneNumber
} from 'react-phone-number-input';
import 'react-phone-number-input/rrui.css'
import 'react-phone-number-input/style.css'

import { userActions } from '../actions/userActions';

class RegisterPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        firstName: '',
        lastName: '',
        username: '',
        password: '',
        email: '',
        country: '',
        gender: '',
        phone: '',
        isAdmin: false
      },
      phone: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    this.handlePhoneChange = this.handlePhoneChange.bind(this);
    this.handleAdminChange = this.handleAdminChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        [name]: value
      }
    });
  }

  handleOptionChange(event) {
    this.setState({
      user: {
        gender: event.target.value
      }
    })
  }

  handlePhoneChange(phone) {
    const { user } = this.state;
    this.setState({
      user: {
        ...user,
        phone: phone
      }
    })
  }

  handleAdminChange(event) {
    const { user } = this.state;
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    console.log(target)

    this.setState({
      user: {
        ...user,
        isAdmin: value
      }
    });
  }

  validateEmail(event) {
    let reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(event);
  }

  handleSubmit(event) {
    event.preventDefault();
    let self = this;

    this.setState({ submitted: true });
    const { user } = this.state;
    const { dispatch } = this.props;
    console.log('userPasswordLength', user.password.length);
    console.log('userEmail', this.validateEmail(user.email));
    console.log('submit', (user.firstName && user.lastName && user.username && user.password.length >= 8 && this.validateEmail(user.email) && user.phone) === true );
    if (user.firstName && user.lastName && user.username && user.password.length >= 8 && this.validateEmail(user.email) && user.phone ) {
      dispatch(userActions.register(user));
    }
  }

  render() {
    const { registering  } = this.props;
    const { user, submitted } = this.state;
    //const phone = this.state.phone;
    console.log('check', user.isAdmin)
    return (
      <div className="col-md-6 col-md-offset-3">
        <h2>Register</h2>
        <form name="form" onSubmit={this.handleSubmit}>
          <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
            <label htmlFor="firstName">First Name</label>
            <input type="text" className="form-control" name="firstName" value={user.firstName} onChange={this.handleChange} />
            {submitted && !user.firstName &&
            <div className="help-block">First Name is required</div>
            }
          </div>
          <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
            <label htmlFor="lastName">Last Name</label>
            <input type="text" className="form-control" name="lastName" value={user.lastName} onChange={this.handleChange} />
            {submitted && !user.lastName &&
            <div className="help-block">Last Name is required</div>
            }
          </div>
          <div className={'form-group' + (submitted && !user.username ? ' has-error' : '')}>
            <label htmlFor="username">Username</label>
            <input type="text" className="form-control" name="username" value={user.username} onChange={this.handleChange} />
            {submitted && !user.username &&
            <div className="help-block">Username is required</div>
            }
          </div>
          <div className={'form-group' + (submitted && user.password.length < 8 ? ' has-error' : '')}>
            <label htmlFor="password">Password</label>
            <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
            {submitted && user.password.length < 8 &&
            <div className="help-block">Password is required</div>
            }
          </div>
          <div className={'form-group' + (submitted && !this.validateEmail(user.email) ? ' has-error' : '')}>
            <label htmlFor="email">Email</label>
            <input type="text" className="form-control" name="email" value={user.email} onChange={this.handleChange} />
            {submitted && !this.validateEmail(user.email) &&
            <div className="help-block">Email is required</div>
            }
          </div>
          <div className={'form-group'}>
            <label htmlFor="admin">Are you an admin?</label>
            <input type="checkbox" className="admin-control" name="admin" value={user.isAdmin} onChange={this.handleAdminChange} checked={this.state.isAdmin} />
            {user.isAdmin ? <p>Yes</p> : <p>No</p>}
            {/*{submitted && !this.validateEmail(user.email) &&*/}
            {/*<div className="help-block">Email is required</div>*/}
            {/*}*/}
          </div>
          {/*<div className={'form-group' + (submitted && !user.gender ? ' has-error' : '')}>*/}
            {/*<label htmlFor="gender">Gender</label>*/}
            {/*<input type="radio" name="gender" value='male' onChange={this.handleOptionChange} checked={this.state.gender === 'male'}  />*/}
            {/*<input type="radio" name="gender" value='female' onChange={this.handleOptionChange} checked={this.state.gender === 'female'} />*/}
            {/*{submitted && !user.gender &&*/}
            {/*<div className="help-block">Gender is required</div>*/}
            {/*}*/}
          {/*</div>*/}
          <div className="form-group" >
            <label htmlFor="phone">Phone</label>
            <Phone
              className="form-control"
              country="UA"
              placeholder="Enter phone number"
              value={user.phone}
              //onChange={phone => this.setState({phone})}
              onChange={this.handlePhoneChange}
              error={!isValidPhoneNumber(user.phone) && 'Invalid phone number'}
              indicateInvalid
            />
          </div>
          <div className="form-group">
            <button className="btn btn-primary">Register</button>
            {registering &&
            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
            }
            <Link to="/login" className="btn btn-link">Cancel</Link>
          </div>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { registering } = state.registration;
  return {
    registering
  };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };