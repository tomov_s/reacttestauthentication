import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {userActions} from '../actions/userActions';

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        firstName: ''
      }
    }
  }

  componentDidMount() {
    this.props.dispatch(userActions.getAll());
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps);
    this.setState({
      user: nextProps.user
    })
  }

  handleDeleteUser(id) {
    return (e) => this.props.dispatch(userActions.delete(id));
  }

  handleChange(event) {
    const {name, value} = event.target;
    const {user} = this.props;
    console.log('user', user.firstName);
    console.log('event', event.target.value);
    this.setState({
      user: {
        // ...user,
        firstName: value
      }
    });
  }

  render() {
    const {user, users} = this.props;
    console.log('user', user);
    return (
      <div>
        <div className="col-md-6 col-md-offset-3">
          <h4>Your profile</h4>
          <h1>Hi {user.firstName}!</h1>
          {/*<h4>First name: <input onChange={this.handleChange.bind(this)} value={this.state.user.firstName}/></h4>*/}
          <h4>First name: {user.firstName}</h4>
          <h4>Last name: {user.lastName}</h4>
          <h4>Username: {user.username}</h4>
          <h4>Email: {user.email}</h4>
          <h4>Phone: {user.phone}</h4>
          <h4>isAdmin: {user.isAdmin ? 'Yes' : 'No'}</h4>
          <p>You're logged in!</p>
          <h3>All registered users:</h3>
          {users.loading && <em>Loading users...</em>}
          {users.items &&
          <ul>
            {users.items.map((user, index) =>
              <li key={user.id}>
                {user.firstName + ' ' + user.lastName}
                {
                  user.deleting ? <em> - Deleting...</em>
                    : user.deleteError ? <span className="error"> - ERROR: {user.deleteError}</span>
                    : <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                }
              </li>
            )}
          </ul>
          }
          <p>
            <Link to="/login">Logout</Link>
          </p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {users, authentication} = state;
  const {user} = authentication;
  return {
    user,
    users
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export {connectedHomePage as HomePage};