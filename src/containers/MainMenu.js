import React, {Component} from 'react';
import {Router, Route, Link, Switch} from 'react-router-dom';
// import {history} from '../helpers/history';
import {Users} from './Users';
import {Profile} from './Profile';
import {HomePage} from './HomePage';
import {LoginPage} from './LoginPage';

import createBrowserHistory from 'history/createBrowserHistory'
import {EditProfile} from "./EditProfile";

const history = createBrowserHistory();

export default class MainMenu extends Component {
  render() {
    return (
      <div>
          <div className="col-md-3"><Link to="/home">Home</Link></div>
          <div className="col-md-3"><Link to="/users">Users</Link></div>
          <div className="col-md-3"><Link to="/profile">Profile</Link></div>
          <div className="col-md-3"><Link to="/login">Logout</Link></div>

          <Route path="/home" component={HomePage}/>
          <Route path="/users" component={Users}/>
          <Route path="/profile" component={Profile}/>
          <Route path="/edit-profile" component={EditProfile}/>
      </div>
    )
  }
}