import React from 'react';
import {Route, Redirect} from 'react-router-dom';

export const PrivateRoute = ({component: Component, ...rest}) => {
  let idStr = 'isAdmin';
  let userStr = localStorage.user;
  if (localStorage.getItem('user')) {
    console.log(333, userStr.includes(idStr));
  }
  console.log(123, localStorage.getItem('user'));
  // let userlocal = localStorage.getItem('user');
  // for (var key in localStorage.user) {
  //   if ( localStorage.user[key] === 'id' )
  //     console.log(123);
  // }
  // userStr.includes(idStr)

  return (
    <Route {...rest} render={props => (
      localStorage.getItem('user') ? <Component {...props} />
        : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
    )}
    />
  )
};