import React, {Component} from 'react';
import {Router, Route, Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {history} from './helpers/history';
import {alertActions} from "./actions/alertActions";
import {PrivateRoute} from "./components/PrivateRoute";
import {HomePage} from './containers/HomePage';
import MainMenu from './containers/MainMenu';
import {LoginPage} from './containers/LoginPage';
import {RegisterPage} from './containers/RegisterPage';
import {EditProfile} from "./containers/EditProfile"
import {Users} from "./containers/Users"
import {Profile} from "./containers/Profile"

// const Authorization = (allowedRoles) => (App) => {
//   return class WithAuthorization extends React.Component {
//
//   }
// };


class App extends Component {
  constructor(props) {
    super(props);

    const {dispatch} = this.props;
    history.listen((location, action) => {
      dispatch(alertActions.clear());
    });
  }

  render() {
    const {alert} = this.props;
    // const Manager = Authorization(['manager', 'admin']);
    console.log('loc',localStorage.user);

    return (
      <div className="jumbotron">
        <div className="container">
          <div className="col-sm-8 col-sm-offset-2">
            {alert.message &&
            <div className={`alert ${alert.type}`}>{alert.message}</div>
            }
            <Router history={history}>
              <div>
                <div className={history.location.pathname === '/login' || history.location.pathname === '/register' ? "display-none" : ''}>
                  <div className="col-md-3"><Link to="/">Home</Link></div>
                  <div className="col-md-3"><Link to="/users">Users</Link></div>
                  <div className="col-md-3"><Link to="/profile">Profile</Link></div>
                  <div className="col-md-3"><Link to="/login">Logout</Link></div>
                </div>

                <PrivateRoute exact path="/" component={HomePage}/>
                <Route path="/login" component={LoginPage}/>
                <Route path="/register" component={RegisterPage}/>
                <Route path="/edit-profile" component={EditProfile}/>
                {/*<Route path="/home" component={HomePage}/>*/}
                <Route path="/users" component={Users}/>
                <Route path="/profile" component={Profile}/>
              </div>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {alert} = state;
  return {
    alert
  };
}

export default connect(mapStateToProps)(App);
