import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import App from './App';
import './App.css';
import {store} from './helpers/store';

//setup fake backend
import {configureFakeBackend} from "./helpers/fake-backend";
configureFakeBackend();

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
